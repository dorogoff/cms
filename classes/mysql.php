<?php

class mysql {

// connection id
    var $connectId;
    var $query;
    var $sql_result;
    var $sql_error;
    var $name;

    function connect($dbhost, $dbuser, $dbpass, $dbName) {
        
        $this->connectId = mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
        mysql_select_db($dbName, $this->connectId);
    }

    function simpleQuery() {
        if(!$this->sql_result = mysql_query($this->query)) {
            $this->sql_error = mysql_error();
            return $this->sql_error;
        }
        return mysql_fetch_row($this->sql_result);
    }
    
    function arrayQuery() {
        if(!$this->sql_result = mysql_query($this->query)) {
            $this->sql_error = mysql_error();
            return $this->sql_error;
        }
        return $this->sql_result;
    }

    function close() {
        mysql_close($this->connectId);
    }

}

?>
