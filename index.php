<?php

include 'conf/conf.php';
include 'classes/mysql.php';
include 'classes/functions.php';

// get url
$mainCategory = $_GET['head'];
$subCategory = $_GET['sub'];
$page = $_GET['page'];


// create mysql database object

$db = new mysql();
//connect to database
$db->connect($config['dbhost'], $config['dbuser'], $config['dbpass'], $config['dbname']);


$page = getHeadPage($db, $mainCategory, $subCategory, $page, $config);


echo $page;
$db->close();
?>