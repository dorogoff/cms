<?php

function getHeadPage(mysql $db, $mainCategory, $subCategory, $pageName, $config) {

    $leftActive = "";
    // prepare page  - main content
    $page = fread($handle = fopen("templates/main.tpl", "r"), filesize("templates/main.tpl"));
    if (!$subCategory) {
        $db->query = "SELECT content, title FROM pages WHERE link='" . mysql_real_escape_string($mainCategory) . "'";
    } else if ($pageName) {
        $db->query = "SELECT content, title FROM pages WHERE link='" . mysql_real_escape_string($pageName) . "'";
    } else {
        $db->query = "SELECT content, title FROM pages WHERE category='" . mysql_real_escape_string($mainCategory) . "' 
                                                         AND subcategory_id>'0' AND link='" . mysql_real_escape_string($subCategory) . "'";
    }

    $content = $db->simpleQuery();
    $page = str_replace("{content}", $content[0], $page);
    $page = str_replace("{page_header}", $content[1], $page);
    $page = str_replace("{home_url}", $config['home_url'], $page);

    // check active items in main menu
    $actives = array();
    switch ($mainCategory) {
        case 'products': $actives[1] = "active";
            break;
        case 'leaky':$actives[2] = "active";
            break;
        case 'air':$actives[3] = "active";
            break;
        case 'controls':$actives[4] = "active";
            break;
        case 'price':$actives[5] = "active";
            break;
        case 'parts':$actives[6] = "active";
            break;
        case 'works':$actives[7] = "active";
            break;
        case 'contacts':$actives[8] = "active";
            break;
        case 'about':$actives[9] = "active";
            break;
    }
    for ($j = 1; $j < 10; $j++) {
        $page = str_replace("{a" . $j . "}", $actives[$j], $page);
    }

    // show submenu
    $db->query = "select title from categories where link='" . mysql_real_escape_string($mainCategory) . "'";
    $leftBlock = $db->simpleQuery();
    //header
    $leftBlockContent = "<h2>" . $leftBlock[0] . "</h2>";
    $db->query = "select title, link from submenu where category='" . mysql_real_escape_string($mainCategory) . "'";
    $leftBlockRes = $db->arrayQuery();
    while ($row = mysql_fetch_array($leftBlockRes)) {
        if ($row['link'] == $subCategory)
            $leftActive = "active";else
            $leftActive = "";
        $leftBlockContent .= "<a href=\"" . $config['home_url'] . "/" . $mainCategory . "/" . $row['link'] . "\" class=\"" . $leftActive . "\">" . $row['title'] . "</a><br/>";
    }



    //define catalog, template from tempates/list_block.tpl
    // only if we have any items in this category
    $block = fread($handle = fopen("templates/list_block.tpl", "r"), filesize("templates/list_block.tpl"));
    if (!$subCategory) {
        $db->query = "SELECT * FROM pages WHERE category='" . mysql_real_escape_string($mainCategory) . "'AND v!='0'";
    } else {
        $db->query = "SELECT * FROM pages WHERE category='" . mysql_real_escape_string($mainCategory) . "'
                                               AND link!='" . mysql_real_escape_string($subCategory) . "' 
                                               AND subcategory_id='" . getSubCatIdByName($db, $subCategory) . "'";
    }
    // if we have a pageName then we don't needed in catalog
    if (!$pageName) {
        $block_content = $db->arrayQuery();


        while ($row = mysql_fetch_array($block_content)) {
            $list_img = $config['home_url']."/img/".$row['img_name'];
            $list_title = $row['title'];
            $list_href = $mainCategory . "/" . getSubCatName($db, $row['subcategory_id']) . "/" . $row['link'];
            $item = $block;

            $item = str_replace("{list_img}", $list_img, $item);
            $item = str_replace("{list_title}", $list_title, $item);
            $item = str_replace("{list_href}", $config['home_url'] . "/" . $list_href, $item);

            $catalog .=$item;
        }
    }
    if (!$item)
        $catalog = "";

    $page = str_replace("{list_block}", $catalog, $page);

    $page = str_replace("{left_block}", $leftBlockContent, $page);
    fclose($handle);
    return $page;
}

function debug() {
    
}

function getSubCatName($db, $id) {
    $db->query = "select link from submenu where id='" . mysql_real_escape_string($id) . "'";
    $res = $db->simpleQuery();
    return $res['0'];
}

function getSubCatIdByName($db, $name) {
    $db->query = "select id from submenu where link='" . mysql_real_escape_string($name) . "'";
    $res = $db->simpleQuery();
    return $res['0'];
}

?>
